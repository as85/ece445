# Grid Independent Water Monitoring/Management System

Team Members:
- Anantajit Subrahmanya (as85)
- Grant McKechnie (granttm2)
- Jayanth Meka (jmeka2)

# Problem

Current irrigation systems offer a way to set timers for scheduled watering. These solutions require a wired connection to an electrical controller and power source, which limits the locations in which irrigation systems can be installed. As climate change strains drought-prone regions that are also subject to power outages, it becomes increasingly important not only to minimize total water consumption, but to also have irrigation systems that work without external power. 

Most systems require a central control unit that uses wires to communicate to all irrigation systems on a property. This method is expensive to install and is at risk to damage that could cause the whole irrigation system to fail. These wires are thin and usually located underground which makes them especially prone to damage from animals like moles or rats. If the irrigation system stops working, it is extremely difficult to know where the source of the failure is.

# Solution

We propose a solution to this problem in the form of a wireless self-powered irrigation system.

Through the utilization of a water powered turbine generator, we will power our whole irrigation system which includes batteries, valve control, and watering timers. The goal of this project is to make it as self contained as possible with an easy installation. If time permits, we would like to also modularize the design so that communication between different irrigation systems is possible. This system can still operate in the event of a power outage which are becoming increasingly common in rural areas during the dry seasons where water management is crucial.

Our device will fit the form factor of existing plumbing couplings (elbows, nipples, and other joints); in other words, it will appear to be a connector between two pipes/hoses. The device will be equipped with a turbine to generate a small current to power a battery which would then power an on-board microcontroller. This microcontroller will be responsible for valve control for watering plants at a given interval. This interval will be initially set by us, but we hope to incorporate soil sensing to enable real time watering interval calculation. The microcontroller should also wirelessly communicate with a base station located nearby, allowing users to manually control the system if needed. The base station, which is powered by a wired DC source, can aggregate data from all nearby valves/sensors and present them to the user.

# Solution Components

## Power

In order to ensure constant power to the microcontroller, we will need to figure out a way to power a battery which will then power the microcontroller. This ensures that the microcontroller always operates within the proper voltage and current requirements. This battery would most likely be a rechargable lithium ion battery housed within a waterproof container. The device that generates the power will be a turbine. The control for this battery management system could use live weather data (provided by the base station) in order to properly manage the charge/discharge cycles of the battery to ensure its longevity. The microcontroller will support two way communication with the base station to help with the control of the system. 

We would most likely want to use a small water turbine generator to power our system. To implement this, we would split the water flow into two paths by using a T-connector. Both paths will have an electronic-controlled valve (see below for details) to control the water intake. One path would act as a bypass, connecting the water intake to the output (which waters the plants). The other path would include a turbine, which slowly recharges the battery, before rejoining main watering line. A microcontroller will control which valves are open and closed based on the state of the system. If we need to produce more power, the first valve will close and the second valve will stay open. If we have enough power and we need more pressure for the irrigation system output, then both values will open.

## Communication

We intend to follow the Zigbee protocol, designating each valve-controller device as a Zigbee End Device, to facilitate communication between the base station and valves. Our chosen microcontroller family includes an integrated wireless modem, with certain SKUs capable of operating at less than 5mA in RX mode. It is important to note that Zigbee mandates the presence of a coordinator device, and in our setup, the irrigation hub acts as the coordinator for the network.

Microcontroller: [Nordic Semiconductor nRF52820](https://www.nordicsemi.com/Products/nRF52820)

Regarding the specifics for power requirements of the wireless system, the modem will sleep most of the time - we are considering tradeoffs between sleep time and responsiveness. Currently, we plan to use a watchdog based wake system for the modem, which will turn on the receiver for less than 50ms once ever 1+ seconds. By our rough estimate, this means that the average current draw in the idle/sleep state is less than 200uA. This number is used for our system-level energy estimates later in the FAQ.

There is significant room for future growth for the wireless side of this project. First, since many crop fields are large (and the zigbee standard's maximum range is 100m), any practical implementation would require extending this range. Zigbee allows for this through the use of routers (essentially repeaters), but these are generally powered from a wired source. If we have extra time, we will explore integrating Zigbee routers to be installed in the field, either powered by the valves themselves, or by some other renewable source of electricity.

## Sensing and Intelligence (Extension)

**If we complete the base requirements of our project, we would like to add sensing to our irrigation system.**

Reusing the existing power generation and storage module described above, a water-powered endcap/valve connected to sensors (moisture, for instance) can be placed anywhere along a pipe. Sensing units will be outfitted with a zigbee transmission unit, to communicate data back to the hub. 

Based on the sensor readings and weather projections, the hub would be able to make decisions on if and when the watering system should be run for individual plants.

# Criterion For Success
 
On the high level, this project will be a success if our network of connected valves can properly manage irrigation cycles without external power dependence. Expanding upon our criterion for success, our system would be initialized with a battery charged to 80% of its theoretical capacity. From the base station, the user would initiate a watering cycle for 10 minutes (say, by pressing a button... for now. We would like to integrate IoT here as an extension of our project). After the watering cycle completes, we would demonstrate that the battery capacity is well-over 80% capacity (in theory, we should be able to get this to full for a sufficiently small battery. If all steps occur without issue, then we will consider our solution a success.

# FAQ:

**Can you actually power the whole system with a water turbine? How will this affect the flow rate?**

Using this device, [a water turbine generator with a 1/2 inch diameter pipe size](https://www.amazon.com/Yosoo-Turbine-Generator-Micro-Hydro-Charging/dp/B00ZCBNNOC), 12VDC output voltage, and maximum current of 220mA (in calculations we use 110mA), it is possible to power all background processes of our system for at least 3 days using conservative estimates. 

This [ball valve](https://ussolid.com/u-s-solid-motorized-ball-valve-1-2-stainless-steel-electrical-ball-valve-with-full-port-9-24-v-dc-2-wire-reverse-polarity.html) requires a minimum of 9VDC with a maximum watt rating of 2W. The valve takes 3-5s to fully open, but we only really need the valve to open at least half way to allow close to 100% water flow. This means that we only need around 1.5-2s for proper valve operation. It will take around 3-4J for one valve to open/close (this number will probably be higher in practice). Ideally we want a 100mAh battery at 3.3VDC which is around 0.33Wh (1188J). Taking into consideration the background processes, valve operation, and turbine generation, we will be able to power the whole system.

We have made many compromises to keep the wireless-component's power consumption as low as possible - specifically, we will have our modem aggressively sleep. The only issue with this is that there will be increased latency (if you try to open a valve remotely, it may take a while before it wakes up and receives the command).

The flow rate will not necessarily be impacted if the system is not charging. This is because each valve-controller will contain a T-junction, which splits the incoming source water into two paths. The first path is the main flow line, opened and closed by a value (this part functions like a regular irrigation system). The second path is the power generation line, which also has an electronically controlled input valve. Since we can close the power generation line at will, flow will not necessarily be impeded by the turbine.

We are making a couple assumptions about the use cases for our product.

1. Irrigation cycle occur regularly with moderate frequency (~3 days between irrigation). This allows us to use each watering cycle to charge for the next watering cycle (in other words, water is not flowing all the time).

2. Irrigation cycles last for 10+ minutes per watering event. This guarantees that we are able to generate enough power during each watering cycle for the device to have enough power to open the charging valve at the beginning of the next charge cycle.

3. Ball valves have >80% of the maximal flow rate, even when they are only 50% open. This informed our power calculations.

4. The microprocessor will sleep for 99% of the time (in other words, RX duty cycle is 1% or lower)

If either assumption 1 or assumption 2 are false, then it is possible that our device will lose power (and need a manual charging valve opening to resume operation).

We very intentionally excluded solar as a secondary power source because it would limit where the smart-valves can be installed (for example, it may be possible that the plant itself may cover the solar cell). Making the location of the solar panel configurable would make the system more cumbersome to install.



**How will we demostrate our project?**

We walked around the outside of the ECEB and found a Zurn Z1320/Z132 hydrant on the side of the building . All we need to access the water is a Zurn Hydrant Key - 3/8" Square Socket which can be obtained by contacting facilities. We can attach our project to this hydrant in order to demo the project.

