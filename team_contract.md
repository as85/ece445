# Fall 2023 ECE 445 Team Contract

**Instructions:** The content of this document should be specific to
your goals and needs. Ideas for the content of each section are provided
as suggestions.

|Grid Independent Water Monitoring/Management System|
|-----------------------------------------------|
| Grant McKechnie, granttm2                     |
| Jayanth Meka, jmeka2                          |
| Anantajit Subrahmanya, as85                   |


ECE 445 is a project-based course. The course includes both team and
individual grades. Project teammates generally all get the same grade
for team assignments based on the expectation that all team members do
their fair share of the work involved. The purpose of this contract is
to lay out the tasks needed for the successful completion of the project
and distribute them in a fair and efficient way to the team members. It
will also discuss how the teammates will work together during the
project and address any issues that come up. A contract that promotes
good teamwork that leads to a successful project should:

> Acknowledge that each team member has commitments and responsibilities outside of ECE 445

> Encourage open communication about challenges that team members are facing, both in and out of ECE 445

> Give team members the benefit of the doubt and the opportunity to explain themselves when something goes wrong and resist jumping to judgment

**Project Description: *Short description of project***

Through the utilization of a water powered turbine generator, we will
power our whole irrigation system which includes batteries, valve
control, and watering timers. The goal of this project is to make it as
self contained as possible with an easy installation. If time permits,
we would like to also modularize the design so that communication
between different irrigation systems is possible. This system can still
operate in the event of a power outage which are becoming increasingly
common in rural areas during the dry seasons where water management is
crucial.

Our device will fit the form factor of existing plumbing couplings
(elbows, nipples, and other joints); in other words, it will appear to
be a connector between two pipes/hoses. The device will be equipped with
a turbine to generate a small current to power a battery which would
then power an on-board microcontroller. This microcontroller will be
responsible for valve control for watering plants at a given interval.
This interval will be initially set by us, but we hope to incorporate
soil sensing to enable real time watering interval calculation. The
microcontroller should also wirelessly communicate with a base station
located nearby, allowing users to manually control the system if needed.
The base station, which is powered by a wired DC source, can aggregate
data from all nearby valves/sensors and present them to the user.

**Project Goals: *If the team is successful in its purpose, what hardware
and software achievements will attest to this?***

1.  Base station is able to communicate with a single valve with simple link/wakeup demo

2.  Recharge valve can open and close multiple times without net charge loss on battery

    a.  No overcharging of the battery

3.  Main valve can open and close multiple times with recharging during the open period

4.  Battery level monitoring from remote base station

5.  Can remotely open/close valves on a cycle to water plants

    a.  3 day cycle (simulated using tools)

**Expectations (ground rules) for each member: *Try to list six or more
minimum expectations. Consider aspects such as preparation,
participation, feedback, responsiveness, etc. Try to explicitly list
anything that could potentially turn into a problem. Find ways to
encourage everyone to communicate (this may also fall under "tasks").***

1.  Accountability: Take responsibility for your tasks, and seek help early if needed.

2.  Effective Meetings: Set clear objectives for meetings to maximize efficiency.

3.  Documentation: Keep records of project activities and communicate task delegation in the group chat.

4.  Communication: Address concerns promptly and maintain open, respectful communication.

5.  Timely Work: Aim to complete tasks ahead of schedule for feedback and improvements.

6.  Proactive Approach: Avoid procrastination and share progress for team feedback.

7.  Continuous Contribution: If tasks are completed early, help others or contribute more to the project.

8.  Responsiveness: Check communication channels daily to stay informed and responsive.

**Roles: *Do you see this team performing well because everyone works
together and contributes equally? Are there certain aspects of the
project that some teammates excel at? Can tasks be spread among
individuals to optimize progress toward the final product?***

In our project, we have established specific leadership roles to ensure
efficient execution. Grant will lead the charging system development for
the turbine, focusing on optimizing power generation for the battery.
Jay will oversee the valve management aspect, particularly addressing
challenges related to torque nonlinearity in the ball valve. His goal is
to ensure precise valve control. Anantajit will be responsible for the
base station and microcontroller components, including wireless
communication, with an emphasis on seamless integration and
functionality.

In addition to our respective leadership roles, we emphasize
collaboration and support within the team. Each team member will
actively contribute to the mechanical component, recognizing our
collective limited expertise in this area. If someone\'s workload
allows, they can assist with tasks in other areas to expedite project
completion. This cooperative approach will facilitate the successful
completion of our project.

**Project Meeting Time(s): *The team will meet at the scheduled team
meeting with TA each week. Can you also preset an ideal time for team
meetings in the lab (your team may need to sign up for lab bench
access)? Is your team interested in meeting to work on other aspects of
the course together such as project research?***

By default, same as lecture time for 445. Arranged each week by us. Meet
in person unless someone is fucked up. Present research at the meeting
weekly to everyone.

**Agenda: *Who will set the agenda? Beyond the weekly meetings with the
TA, what will the team do to ensure that it stays on track during the
semester? When a decision needs to be made, will it be approved by
consensus or majority vote? Will a team member be appointed to keep
records?***

In our project\'s meeting protocol, we have established a structured
approach to setting agendas. To ensure equal participation and
responsibility among team members, we will rotate the task of agenda
setting. Each team member will take turns setting the agenda for our
upcoming meetings, ensuring a diverse range of topics and perspectives
are addressed. Additionally, to maintain meeting efficiency and
accountability, we will designate, during each meeting (n), the
individual who will lead and keep discussions on track for the
subsequent meeting (n + 1). This dual role allows us to streamline our
meetings and ensures that the agenda items are addressed effectively by
the designated leader. This approach promotes equitable participation
and effective meeting management within our team.

**Process and penalties for dealing with team issues: What happens when
ground rules are broken? Who intervenes? What happens if the situation
escalates? Always remember not to jump to judgment. Give group members
the benefit of the doubt and the opportunity to explain themselves when
something first goes wrong. TAs and instructors are available to help
resolve issues.**

To uphold a standard of punctuality and professionalism during our
project engagements, we have implemented a protocol regarding tardiness.
If a team member is delayed by 10 or more minutes without a valid and
acceptable explanation, they commit to a physical activity, such as
performing 10 push-ups, as a gesture of accountability. This practice
encourages timely attendance and ensures that our collaborative efforts
are conducted with respect for one another\'s time and commitment.

**End-of-term agreement on using final peer assessment for grade
adjustment: Do you believe that this contract should hold your team
accountable to its contents or that it may hold little value? There will
be two formal peer assessments this semester. The first is used only to
provide honest, constructive feedback to each team member. The second
peer assessment affects a teammate's grade. Without accountability, many
promises go by the wayside.**

We believe that this contract should hold our team accountable to its
contents as it ensures the integrity of the peer assessment process and
encourages commitment to our promises.

**Signatures: Iterate on this document until everyone is comfortable with
its contents and signs (it is okay to type your printed name as your
digital signature).**

*I affirm that I participated in generating this team charter and that I
will abide by its contents to the best of my ability. Furthermore, I
understand that failure to meet the expectations expressed here can lead
to the stated consequences.*

netID: granttm2 (digital) Signature: Grant McKechnie Date: 9/11/2023

netID: as85 (digital) Signature: Anantajit Subrahmanya Date: 9/11/2023

netID: jmeka2 (digital) Signature: Jayanth Meka Date: 9/11/2023
