---
title: "Team Contract Fulfillment"
author: Anantajit Subrahmanya, Grant McKechnie, Jay Meka
date: November 28, 2023
geometry: margin=1in
output: pdf_document
---

## Project Goals

Our original project goal was to build a self-powered irrigation system, which itself can be divided into two subprojects. The first is a wireless valve-operation system, which would use a microcontroller and a Bluetooth connection to open and close ball valves to control the flow of water. The second was using a turbine to generate electricity and recharge a battery while water was flowing. During the semester, we were successfully able to open and close the two ball valves using an ESP32-C6 microcontroller. Unfortunately, due to some issues with our battery charging IC, the battery charging process was not successful as originally designed. We are currently exploring some workarounds by using pre-fabbed battery charging circuitry, but we do face a significant risk of not achieving this goal of our project. As a sidenote, some of the other goals (not high level requirements) will be met. For one, the device attaches to existing plumbing couplings using a 1/2" female connector, which was one of our goals, and the other side is a standard 1/2" male PVC connector.

**Summary**

1. [GOAL COMPLETED] A base station is able to communicate with a single valve

2. [GOAL PARTIALLY COMPLETED] Recharge valve can open and close multiple times, but battery is currently not recharging

3. [GOAL PARTIALLY COMPLETED] Main valve can open and close multiple times, but recharging does not function as intended

4. [GOAL PARTIALLY COMPLETED] We are able to measure the battery level from the remote base station, with low accuracy.

5. [GOAL COMPLETED] Can remotely open/close valves on a cycle to water plants

## Expectations

1. Accountability - this was only partially followed. We often missed self-set deadlines, and we frequently needed to 'cover' for each other throughout this semester.

2. Effective meetings - we followed this moderately well. Meetings had purposes set and we often achieved what we set out to do at the start of each meeting.

3. Documentation - for the most part, we updated our notebooks regularly with our activities.

4. Communication - somewhat related to accountability, but many of the missed deadlines and other issues could have been resolved with better communication. We could have followed this guideline better.

5. Timely Work - as alluded to in the accountability and communication sections, this was a major issue and we could have been better about hitting our milestones on time.

6. Proactive approach - this was also a problem, related to timely work, and does explain why we were unable to complete all of our milestones on time.

7. Continuous Contribution - this was done very well by our team. Although we each had set responsibilities, team members regularly helped each other with tasks to keep things running smoothly.

8. Responsiveness - for the most part, team members responded to messages within 24 hours.

## Roles

As we planned, the leadership roles were aligned by subsection; Grant led the power subsystem, Anantajit led the microcontroller/software component and Jay led the valve subsystem. One change from the planned roles was that Grant played a significant role in the valve subsystem design as well, since there were some power conversions involved, and Jay used the additional time freed up to work on the physical design.

Due to the nature of KiCAD work (specifically the lack of good version control features, such as merging) we found it difficult to work on the PCB at the same time. For this reason, division along subsystems was an excellent idea. Our workflow involved each team member designing their own subsystem on a separate KiCAD project file in the shared GitLab repository. Anantajit and Grant (alternating PCB orders) then would combine the three subsystems to send out the weekly PCB order, often tweaking some of the routing to make everything fit.

## Agenda

Our team usually agreed with all of our todo items and goals; that is, we never had disagreements about _what_ needed to be done. Our agenda was usually deadline-driven based on the ECE445 calendar. For instance, if the PCB order was due on Tuesday, we would set our internal deadline to Monday so Anantajit/Grant had enough time to combine the design before sending it out for fabrication.

## Team Issues

Punctuality was one issue that we spelled out in our team contract. However, this was not a major issue for us, as we rarely held formal synchronous meetings with all three members present.

One oversight on our part was not expecting timely work to be a major issue - we did not set aside an action for this in our team contract.
