# A collection of smaller updates

10/2/23 - Created a rough schematic of the C6 variant for the project design document. We're starting by ordering the C6 version of the chip anyway, since the H2 is not available. I also added the images for the LaTeX.

10/6/23 - Worked on the Microcontroller PCB design and completed the first draft of the design. Note that we're planning to use an external programmer for the initial version, so I simply included JTAG and UART header pins.

10/8/23 - Modified the proposal on Gitlab for reviisions