PCBNEW-LibModule-V1  2023-10-31 20:02:52
# encoding utf-8
Units mm
$INDEX
SRN4012BTA4R7M
$EndINDEX
$MODULE SRN4012BTA4R7M
Po 0 0 0 15 65415d6c 00000000 ~~
Li SRN4012BTA4R7M
Cd SRN4012BTA-4R7M
Kw Inductor
Sc 0
At SMD
AR 
Op 0 0 0
T0 0.000 -0 1.27 1.27 0 0.254 N V 21 N "L**"
T1 0.000 -0 1.27 1.27 0 0.254 N I 21 N "SRN4012BTA4R7M"
DS -2 2 2 2 0.1 24
DS 2 2 2 -2 0.1 24
DS 2 -2 -2 -2 0.1 24
DS -2 -2 -2 2 0.1 24
DS -3.25 -3.25 3.25 -3.25 0.1 24
DS 3.25 -3.25 3.25 3.25 0.1 24
DS 3.25 3.25 -3.25 3.25 0.1 24
DS -3.25 3.25 -3.25 -3.25 0.1 24
DS -0.4 -2 0.4 -2 0.2 21
DS -0.4 2 0.4 2 0.2 21
DS -2.8 -0 -2.8 -0 0.1 21
DS -2.7 -0 -2.7 -0 0.1 21
DA -2.75 -0 -2.800 -0 -1800 0.1 21
DA -2.75 -0 -2.700 -0 -1800 0.1 21
$PAD
Po -1.500 -0
Sh "1" R 1.500 4.500 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.500 -0
Sh "2" R 1.500 4.500 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE SRN4012BTA4R7M
$EndLIBRARY
