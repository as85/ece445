# Introduction

## Problem

Current irrigation systems offer a way to set timers for scheduled watering. These solutions require a wired connection to an electrical controller and power source, which limits the locations in which irrigation systems can be installed. As climate change strains drought-prone regions that are also subject to power outages [1], it becomes increasingly important not only to minimize total water consumption, but to also have irrigation systems that work without external power. Most systems require a central control unit that uses wires to communicate to all irrigation systems on a property. As you can see in Figure 1, this control unit is usually located within a home and is powered from a wall plug. Residents are able to set their own watering cycles and monitor water usage. This method is expensive to install and is at risk of damage that could cause the whole irrigation system to fail. The wires from the control unit are thin and usually located underground which makes them especially prone to damage from animals like moles or rats. If the irrigation system stops working, it is extremely difficult to know where the source of the failure is. There are solar powered irrigation systems on the market, but they do not completely solve the problem. Fully solar powered systems do not have a reliable source of power as solar is highly dependent on the weather. 

## Solution

We propose a solution to this problem in the form of a wireless self-powered irrigation system. Through the utilization of a water-powered turbine generator, we will power our whole irrigation system which includes batteries, valve control, and watering timers. The goal of this project is to make it as self-contained as possible with an easy installation. If time permits, we would like to also modularize the design so that communication between different irrigation systems is possible. This system can still operate in the event of a power outage which is becoming increasingly common in rural areas during the dry seasons where water management is crucial. Our device will fit the form factor of existing plumbing couplings (elbows, nipples, and other joints); in other words, it will appear to be a connector between two pipes/hoses. The device will be equipped with a turbine that will generator power to charge a battery which will then power on onboard microcontroller. This microcontroller will be responsible for valve control for watering plants at a given interval. This interval will be initially set by us, but we hope to possibly incorporate soil sensing to enable real-time watering interval calculation. The microcontroller should also wirelessly communicate with a base station located nearby, allowing users to manually control the system if needed. The base station, which is powered by a wired DC source, can aggregate data from all nearby valves/sensors and present them to the user. Our solution will offer a reliable power source that is not subject to the weather, have less wires that could easily be damaged, and will be simply an attachment to existing plumbing rather than a full external system. 

# Visual Aid

![](hld.png)

Note that our solution will be parallel to the ground, this aid only helps with the visualization. Also, components are not to scale.

# High-level Requirements List

1. The device must be able to open and close valves based on remotely-issued commands. The valve should change state within 20 seconds of receiving the issued command. We define 'open' as allowing over 50% of the waterflow into the pipes, and 'closed' as letting no waterflow into the pipes.
2. The battery used to power the project must have a net positive charge increase over the course of a watering cycle. A typical watering cycle is 10 minutes in length. This can be tested by measuring the battery percentage before and after running one watering cycle.
3. The device must be able to operate for at least 12 hours on a single charge (starting with 90% battery capacity). Note that for testing this requirement during the demo, the device must also be able to measure the battery charge level.

# Design

## Block Diagram

![](block.png)
 
## Subsystem Overview

Power Generation: This subsystem includes the water turbine generator that generates power from the water flow. The power generated from the turbine is not perfectly DC as the voltage is dependent on the flow rate, so the power must pass through a DC-DC converter that has a voltage regulator before going to a battery charging circuit. The charged battery will power the microcontroller and the valve control circuit through the 3.3V DC bus. There will be a battery monitoring IC that sends information to the microcontroller about the state of the battery. This will be charge data (percent charged) as well as voltage. 

‘PCB’: This subsystem will send and receive data (valve status and water timing) to and from the base station, process that data, and then send out PWM signals to the valve control circuit and use DC-DC converter within the power generation subsystem to power the microcontroller and all wireless operations. The transceiver and processor will be powered by the 3.7Vdc battery so there may need to be some sort of voltage step up or step down based on the transceiver and processor that we pick - this will be handled by the power subsystem as well. The processor will also measure the voltage of the battery and make decisions based on how charged/discharged the battery is. The power subsystem will compute these measurements and communicate them to the 'PCB' subsystem over an i2c bus.

Valve Control Circuit: The valves require a minimum of 9Vdc so there has to be a DC-DC step up to 12Vdc if we want to perform any pulse width modulation. These will be provided through the 12Vdc bus from the power subsystem. The valve control circuitry will take signals from the microcontroller subsystem (note that in the block diagram, this is called the 'processor') and use them to control the valve actuation through pulse width modulation signals.

### Requirements

Valve Subsystem: 

- The watering valve must open, close and maintain state depending on the signals transmitted to the valve subsystem
- The battery consumption for each valve operation must be under 10% of the net battery capacity

Power Subsystem:

- If the battery is fully charged (4.2V across the terminals), then any charging operation must halt
- If the battery is near empty (3.4V across the terminals), then all valves must close
- The power supply to the battery charger circuit must provide a voltage in the range of 4.7 - 5.5 volts for a current load of up to 300mA while the watering cycle is active

PCB Subsystem:

- Maintain radio duty cycle of <50ms RX-on time per 950ms of deep sleep time without connection reset.
- Ability to detect wake commands during RX-on.
- Transfer >10 bits of information per second (TX) and receive >10 bits of information per second (RX) while drawing <100mA of current.
- The device must be able to maintain a wireless connection with the base station for at least 12 hours without any charging operation
- The device must be able to pair with a laptop (base station) over a wired connection
- The device must be capable of generating a PWM signal which can be amplified to control a servo motor

# Tolerance Analysis

## Wireless Power Consumption Computations and Tolerances

Let $D$ be the duty cycle of the modem (otherwise the modem will be in a deep sleep mode), $I_H$ is the max current for receiving mode for the modem and $I_L$ is the deep sleep current. For our calculations, we assume $D < 0.05$, $I_H < 10$mA, and $I_L <$ 100uA. Thus, our average current can be computed with the equation:
$$I_{avg} = D I_H + (1 - D) I_L$$, which after applying all unit conversions equates to around $0.2mA$ on average for operation. Assuming that our battery outputs a steady 3V (which is the $V_{in}$ requirement for all modems we are considering), this means that for three days of sleep-mode operation, the battery consumption $\approx 5$ milliamp hours per day we wish to operate in sleep mode. Thus, if we used a 100mAh battery, then idle behavior for 5 days would use no more than 25% of the capacity of the battery. We predict that with software, we can further lower the duty cycle. However, the current measurements will have to be measured in the lab, since additional components on the PCB may have a passive drain that we cannot predict at this point in time.
Since TX communication will only take place while the system is recharging, we do not plan to compute the power consumption requirements (negligible at <5 mA 
Note: we expect that our actual duty cycle will be closer to 1%, meaning that we have allocated around 5 times the actual amount of power consumed for wireless communication

## Power Generation Computations and Tolerances

Based on the documentation [2], the water turbine generator can provide a voltage of 12V at a current above 220mA. The current is heavily dependent on water flow rate and pressure. We would like to study this relationship to get a better estimate on the power generation capabilities of this turbine. If we assume the worst case scenario and say that the turbine can only produce 50mA at a voltage of 12Vdc, then the turbine can generate 0.6W of power. For a 20 minute watering cycle (which is typical for grass watering), this would mean that we could produce 720 Joules of energy ($1200\cdot 0.6W = 360J$). The battery that we need to charge is 100mAhr at 3.7V which is 0.37Whrs ($\text{0.1Ahr}\cdot3.7V=0.37Whr$). This means that it would take 0.616 hours ($\frac{0.37Whr}{0.6W}$ or 37 minutes to fully charge the battery if it is initially dead. This equates to roughly 2 watering cycles.

## Valve Operation Computations and Tolerances

Based on the documentation and videos provided, each valve is able to operate effectively over a 9Vdc to 24Vdc range with a wattage of 2 W. Since we plan to operate using a 12Vdc voltage source, the current draw will be approximately 170mA. Although the valve takes approximately 7 seconds to open, we will not be opening the valve more than halfway at any given time, meaning opening/closing the valve will take 3.5 seconds each. The energy needed to open or close a single valve once is 7 Joules of energy. Opening and closing both valves would therefore consume 28 Joules of energy. Since the battery can store 0.37Whrs, we could open and close the valves 47 times from a full battery, meaning we will have much more than the necessary amount of energy needed. However, this needs to be further tested as we expect the energy consumption of the valves to be higher.

The biggest risk to the successful completion of the project is the water turbine. Although there are datasheets for the components we are planning to use, we cannot know the power generation capabilities of these systems without testing.

# Ethics and Safety

Bluetooth key exchanges for pairing work over a wired connection with the base station. When the field device is connected to any base station, it will enter a "pairing mode" where it conducts the pairing process and any key exchanges over the wired connection. Secure pairing is especially important because our device is essential for plant life, and over/underwatering can cause death. Once the device is unplugged, it will automatically switch into wireless mode, where a packet is sent/received every 4 seconds to reaffirm a strong connection. Loss of connection is defined as a field device being inaccessible for more than one minute, at which point the user will be notified. 

To prevent overwatering, our protocol allows the base station to only initiate watering cycles for set periods of time. For instance, if the wireless connection is lost in the middle of a watering cycle, the cycle would complete automatically after the time limit is reached. We cannot prevent underwatering (notifying the user is our best option).

The wired security component is limiting in the sense that if an intruder was able to gain physical access to the device, and they had their own base station, they would be able to become the 'master' of the field device. We are not covering this security case though, beyond alerting the user that the field device has changed to a different network (in other words, we are not taking preemptive measures to prevent this from happening).

This project involves inherent safety risks due to the proximity of water and electricity. Accidental electrical shock is a potential hazard when working with the system after water has been running through it. Although the currents and voltages are low, safety precautions must be taken to avoid such incidents. Safety measures will include insulating and waterproofing components, clear warnings, and proper training for users. When charging the batteries from an external source, make sure not to overcharge for an extended period of time. The voltage of the battery should not surpass 4.2V for over 30 minutes. The battery should be checked before inserting into the device for any puffiness or deformities. The battery should be dry before placing them into the device to prevent any internal damage or rusting. 

The enclosure for our microcontroller and power system must be waterproofed and tested before installation. The most problematic part of our project will be the turbine itself. Our primary goal is to fully waterproof the turbine so no water leaks from the interconnections within the system. This will ensure that no water can possibly get into the electronics. All people present for the demo must be at least 6 feet away from the device when the water is running. Once the water is turned off, we must wait at least 2 minutes before handling any electronics. We will be able to tell if there are any power supply problems based on the data sent to the base station.

## References

[1] U.S. Geological Survey, "Droughts and Climate Change," \emph{U.S. Geological Survey}, Nov. 3, 2015. [Online]. Available: \url{https://www.usgs.gov/science/science-explorer/climate/droughts-and-climate-change}
[2] Amazon, "Yosoo DC Water Turbine Generator Water 12V DC 10W Micro-Hydro Water Charging Tool", \emph{Yosoo}, Oct. 23, 2019. [Online]. Available: \url{https://www.amazon.com/Yosoo-Turbine-Generator-Micro-Hydro-Charging/dp/B00ZCBNNOC}
