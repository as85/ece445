# ECE445 Design Document Tasks & Timeline

[9/26] Introduction

  [@grant] Problem and Solution: content doesn't really need to be created. I do think we should significantly modify the existing text... one question people keep bringing up is "why no solar" to which the answer is "because no wires allowed" (because it's easy isn't a valid excuse). I think buried pipes/obstruction by the plants themselves is an excellent way to motivate this.

  [@grant] Visual Aid: I think that we are good for our visual aid. It might be useful to add something which explains how the wireless components and power flow work (so basically another diagram).

  [@anantajit] High-level Requirements List: Let's see if we can quantify our requirements better (with a table, maybe?) A good thing to note for power consumption would be to hook it up to a meter (or have one on-board, which would be even better) and display that. I don't think our previous requirements were all very testable.

[9/27] Design

  [X] Block Diagram: I think this is good. @Grant check it off here if it's completed.
  
  [ ] Physical Design: HUGE TODO! Goal: rough draft should be done by Tuesday night. 
  
  [@grant X] Power Subsystem: Grant TODO. Please have specific ICs which you want to use. Also, for a 'alternate' approach, maybe cite circuit designs? 

    [@grant X] Requirements

  [@anantajit] Wireless/Microcontroller Subsystem: Anantajit TODO. I already have my high level ideas for this captured in my notebook, just need to translate it into a design doc.

    [@anantajit] Requirements

  [ ] Valve Subsystems: I see two options - either DIY a powered ball valve, or buy one. I think DIY is a bit difficult but is worth it, given the cost and complete lack of datasheets. At least if we make our own, we'll have more freedom to debug. Two approaches need to be presented here.

    [ ] Requirements
  
[9/27] Cost and Schedule

  [@grant to do] Cost Analysis

    [@grant to do] Labor - bullshit numbers

    [ ] Parts - for now, everyone just track parts of each subsystem. Assemble once all the subsystems are finalized.

      [9/23] Someone please make a table template so we all can make use of it

  [ ] Schedule - again, can only be done after all the subsystems are put together. We should do this collaboratively... meeting required this Wednesday.

[9/27] Ethics and Safety

  [@grant to do] Write some BS here

  [@grant to do] Address the waterproofing issue

[9/28 @ 5pm] Citations

  [ ] Collect your own citations. We will assemble them together during our Wednesday meeting (or tbh can be done on Thursday)

