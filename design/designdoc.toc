\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}Design}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Power system with regulated turbine}{4}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Block Diagram}{4}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Power System with unregulated turbine}{5}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Microcontroller Subsystem Design}{9}{subsection.2.4}%
\contentsline {subsubsection}{\numberline {2.4.1}Integrated Ultra Low Power Microcontroller/Modem}{13}{subsubsection.2.4.1}%
\contentsline {subsubsection}{\numberline {2.4.2}Accessible WiFi/BLE Modem/Microcontroller Hardware}{15}{subsubsection.2.4.2}%
\contentsline {section}{\numberline {3}Cost and Schedule}{18}{section.3}%
\contentsline {section}{\numberline {4}Discussion of Ethics and Safety}{19}{section.4}%
\contentsline {subsection}{\numberline {4.1}Security}{19}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Electrical and Water Safety}{19}{subsection.4.2}%
\contentsline {section}{\numberline {5}References}{20}{section.5}%
