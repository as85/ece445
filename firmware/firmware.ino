/*
    Single device firmware package for ECE445.
    Author: Anantajit Subrahmanya

    Adapted from the following:
    Video: https://www.youtube.com/watch?v=oCMOYS71NIU
    Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleNotify.cpp
    Arduino ESP32 Port by Evandro Copercini
*/

// Parameters to change for the prod board
#define CONNECTION_STATUS_PIN LED_BUILTIN
#define ANALOG_MEAS_PIN 1
#define VALVE_A_FW_PIN 11
#define VALVE_A_BW_PIN 12
#define VALVE_B_FW_PIN 13
#define VALVE_B_BW_PIN 14
#define MIN_BATTERY_PERCENT 30.0
#define BATTERY_EMPTY 3.1
#define BATTERY_FULL 4.0

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

// Bluetooth-related structures
BLEServer *pServer = NULL;
BLECharacteristic *pTxCharacteristic;
BLECharacteristic *pRxCharacteristic;
// For now, counter is just a raw byte
uint8_t counter = 0;
float battery = 0;
float voltage = 0;

char rxmsg[16] = "";

const unsigned int valve_pins[] = {VALVE_A_FW_PIN, VALVE_A_BW_PIN, VALVE_B_FW_PIN, VALVE_B_BW_PIN};
int valve_states[] = {LOW, LOW};

bool deviceConnected = false;
bool oldDeviceConnected = false;

const int ledPin = CONNECTION_STATUS_PIN;

#define SERVICE_UUID "104943ba-7fea-4b23-a52e-167e3b9f4292"
#define CHARACTERISTIC_UUID_RX "0e24a6ea-7910-4aee-85d1-b77c83475dd9"
#define CHARACTERISTIC_UUID_TX "0e24a6ea-7910-4aee-85d1-b77c83475dd9"


class MyServerCallbacks : public BLEServerCallbacks {
  void onConnect(BLEServer *pServer) {
    deviceConnected = true;
  };

  void onDisconnect(BLEServer *pServer) {
    deviceConnected = false;
  }
};

class MyCallbacks : public BLECharacteristicCallbacks {
  void onRead(BLECharacteristic *pCharacteristic) {
    // Called when phone requests a read
  }

  void onWrite(BLECharacteristic *pCharacteristic) {
    String rxValue = pCharacteristic->getValue();

    if (rxValue.length() > 0) {
      if (rxValue.length() >= 15) {
        Serial.println("Command is too long to process.");
      } else {
        if (rxValue.equals(String("start recharge"))) {
            valve_states[0] = HIGH;
        } else if (rxValue.equals(String("start watering"))) {
            valve_states[1] = HIGH;
        } else if (rxValue.equals(String("stop recharge"))) {
            valve_states[0] = LOW;
        } else if (rxValue.equals(String("stop watering"))) {
            valve_states[1] = LOW;
        } else if (rxValue.equals(String("start"))) {
            valve_states[0] = HIGH;
            valve_states[1] = HIGH;
        } else if (rxValue.equals(String("stop"))) {
            valve_states[0] = LOW;
            valve_states[1] = LOW;
        } else {
            Serial.println("Invalid command. Try [start/stop] ([watering/recharge])");
        }
      }
    }
  }
};


void setup() {
  // turn off the status LEDs
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  // setup the digital output pins for controlling the valves
  for(int i = 0; i < 4; i++){
    pinMode(valve_pins[i], OUTPUT);
  }

  // Setup the local serial console
  Serial.begin(115200);

  // Create the BLE Device
  BLEDevice::init("Wireless Valve");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic to notify that the device state has change
  pTxCharacteristic = pService->createCharacteristic(
    CHARACTERISTIC_UUID_TX,
    BLECharacteristic::PROPERTY_NOTIFY);

  pRxCharacteristic = pService->createCharacteristic(
    CHARACTERISTIC_UUID_RX,
    BLECharacteristic::PROPERTY_WRITE);

  // Setup TX/RX/NOTIFY
  pRxCharacteristic->setCallbacks(new MyCallbacks());
  pTxCharacteristic->addDescriptor(new BLE2902());
  pTxCharacteristic->setValue("Initializing...");

  // Start the service
  pService->start();


  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}

void loop() {
  // empty: 1960
  // full: 2500
  voltage = (analogRead(ANALOG_MEAS_PIN) * 3.3 / 4096.0) * 2 + 0.1;
  battery = (voltage - BATTERY_EMPTY) * 100.0 / (BATTERY_FULL - BATTERY_EMPTY);

  digitalWrite(VALVE_A_FW_PIN, valve_states[0]);
  delay(100);
  digitalWrite(VALVE_B_FW_PIN, valve_states[1]);
  delay(100);
  digitalWrite(VALVE_A_BW_PIN, valve_states[0] == HIGH ? LOW : HIGH);
  delay(100);
  digitalWrite(VALVE_B_BW_PIN, valve_states[1] == HIGH ? LOW : HIGH);
  delay(100);

  if (deviceConnected) {
    digitalWrite(ledPin, HIGH);
    delay(500);
    pTxCharacteristic->setValue("Charge: " + String(battery) + "%");
    delay(500);  // bluetooth stack will go into congestion, if too many packets are sent
    pTxCharacteristic->notify();
    delay(5000);
    pTxCharacteristic->setValue("Battery Voltage: " + String(voltage) + "V");
    delay(500);  // bluetooth stack will go into congestion, if too many packets are sent
    pTxCharacteristic->notify();
  } else {
    digitalWrite(ledPin, LOW);
    delay(1000);
  }

  if(battery < MIN_BATTERY_PERCENT) {
    valve_states[0] = LOW;
    valve_states[1] = LOW;
    delay(1000);
  }

  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500);                   // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising();  // restart advertising
    Serial.println("start advertising");
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }
}
